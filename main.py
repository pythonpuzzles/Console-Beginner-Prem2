
def example_a():
    print('\nExample A')
    print('~~~~~~~~~')

    word1 = input("Enter a word: ")

    word2 = input("Enter a second word: ")

    word3 = input("Enter a third word: ")

    # String Join()
    message = "Your three words are: " + " ".join([word1, word2, word3])
    print(message)


# Puzzle A - Totally Baked
#
# Write a program that asks "What 3 ingredients do we need?: "
# Get 3 responses from the user
# Using " ".join() output "Shopping List: {ingredient1}/{ingredient2}/{ingredient3}"
def puzzle_a():
    print('\nPuzzle A')
    print('~~~~~~~~~')


def example_b():
    print('\nExample B')
    print('~~~~~~~~~')

    order = "Shopping Cart Contains: lubricant, bead necklace, asprin"
    print(order)
    print("Lets be honest, you don't need the asprin... What should we swap it with?: ")
    user_input = input(">")

    print(order.replace("asprin", user_input))


# Puzzle B - Perks of the job
#
# Write a program that asks the user for an item to be placed in their shopping cart
#   "What item would you like to be in your shopping cart?: "
# Then tell the user their item is out of stock
#   "Sorry, {user_item} is out of stock"
# Then ask the user to replace their item with something else.
#   "What can we swap {user_item} for instead?: "
# Finally, display the shopping cart with their swapped item "Shopping Cart: {swapped_item}"
def puzzle_b():
    print('\nPuzzle B')
    print('~~~~~~~~~')


def example_c():
    print('\nExample C')
    print('~~~~~~~~~~~')

    print("You: *Opens Bottle*")
    print("...SHAZAM!!... A Magic Geneie appears!...")
    print("Genie: I am a Genie, thanks for freeing me!")

    print("\nGenie: You can have 3 wishes, what will they be?")

    wish1 = input("Wish 1:")

    wish2 = input("Wish 2:")

    wish3 = input("Wish 3:")

    print("\nGenie: Sure! *snaps fingers*")
    print(f"Your wishes: \n {wish1} \n {wish2} \n {wish3}")
    print("\n ALL CAME TRUE!")


# Puzzle C - Spice World
#
# Write a program like the Genie example above, but with a lazy genie that only gives 1 wish
# The genie will ask to replace the wish with an easier item.
# "Can I replace "{wish}" with a Space Girls CD instead?"
# Then no matter what the user inputs, the genie will say
# "You need to space up your life, mate"
def puzzle_c():
    print('\nPuzzle C')
    print('~~~~~~~~~~~')


if __name__ == '__main__':
    
    # Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
