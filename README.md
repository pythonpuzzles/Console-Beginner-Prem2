# Python Console Beginners Premium Puzzle 2
## Thank you, for contributing to the channel! 
### Your money helps support me and allows me to keep making these learning puzzles.

### View the YouTube Video: 


## Puzzles
In main.py, I have commented out some starting code for these puzzles. <br />
Write your code in these sections and uncomment them. <br />
I have provided some examples of similar functions to what each puzzle is asking for. Learn to read the code and modify it. <br />

At the very bottom, I have listed the functions. Comment and uncomment them, to run them one at a time.

```
# Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
```

### A - Totally Baked
Write a program that asks "What 3 ingredients do we need?: "<br />
Get 3 responses from the user<br />
Using " ".join() output "Shopping List: {ingredient1}/{ingredient2}/{ingredient3}" <br />


### B - Perks of the job
Write a program that asks the user for an item to be placed in their shopping cart <br />
"What item would you like to be in your shopping cart?: " <br />
Then tell the user their item is out of stock <br />
"Sorry, {userItem} is out of stock" <br />
Then ask the user to replace their item with something else. <br />
"What can we swap {userItem} for instead?: " <br />
Finally, display the shopping cart with their swapped item "Shopping Cart: {swappedItem}" <br />


### C - Spice World
Write a program like the Genie example above, but with a lazy genie that only gives 1 wish <br />
The genie will ask to replace the wish with an easier item. <br />
"Can I replace "{wish}" with a Space Girls CD instead?"<br />
Then no matter what the user inputs, the genie will say <br />
"You need to space up your life, mate" <br />


# Good Luck!